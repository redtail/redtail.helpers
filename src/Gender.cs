using System;
using System.Reflection;

namespace RedTail.Helpers
{
	public enum Gender
	{
		[EnumModel(Name = "Male", Value = "M")]
		Male = 1,

		[EnumModel(Name = "Female", Value = "F")]
		Female = 2
	}

	public static class GenderExtensions
	{
		public static String GetPronounDependentPossessive(this Gender e)
		{
			if (e == Gender.Female)
				return "Her";

			return "His";
		}

		public static String GetPronounIndependentPossessive(this Gender e)
		{
			if (e == Gender.Female)
				return "Hers";

			return "His";
		}

		public static String GetPronounObject(this Gender e)
		{
			if (e == Gender.Female)
				return "Her";

			return "Him";
		}

		public static String GetPronounReflexive(this Gender e)
		{
			if (e == Gender.Female)
				return "Herself";

			return "Himself";
		}

		public static String GetPronounSubject(this Gender e)
		{
			if (e == Gender.Female)
				return "She";

			return "He";
		}
	}
}
