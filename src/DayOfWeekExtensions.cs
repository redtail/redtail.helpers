using System;

namespace RedTail.Helpers
{
	public static class DayOfWeekExtensions
	{
		public static Boolean IsBetween(this DayOfWeek a, DayOfWeek start, DayOfWeek end)
		{
      if (start <= end)
        return a >= start && a <= end;

      return a >= start || a <= end;
		}
	}
}
