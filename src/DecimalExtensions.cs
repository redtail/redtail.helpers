using System;

namespace RedTail.Helpers
{
	public static class DecimalExtensions
	{
		public static String ToCurrencyString(this Decimal a)
		{
			return String.Format("{0:C}", a);
		}
	}
}
