using System;

namespace RedTail.Helpers
{
	public static class DateTimeExtensions
	{
		public static Boolean IsBetween(this DateTime a, DateTime start, DateTime end)
		{
      if (start <= end)
        return a >= start && a <= end;

      return a >= start || a <= end;
		}
	}
}
