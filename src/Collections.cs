using System;
using System.Collections.Generic;
using System.Linq;

namespace RedTail.Helpers
{
  public static class Collections
  {
    public static List<State> States { get; }
			= Enum.GetValues(typeof(State)).Cast<State>().ToList();
  }
}
