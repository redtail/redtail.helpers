using System;

namespace RedTail.Helpers
{
	public static class DoubleExtensions
	{
		public static String ToPercentString(this Double a)
		{
			return String.Format("{0:P}", a);
		}
	}
}
